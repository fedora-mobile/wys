Name:       wys
Version:    0.1.9
Release:    1%{?dist}
Summary:    A daemon to bring up and take down PulseAudio loopbacks for phone call audio

License:    GPLv3+
URL:        https://source.puri.sm/Librem5/wys
Source0:    https://source.puri.sm/Librem5/wys/-/archive/v%{version}/wys-v%{version}.tar.gz

Source1:    wys.service
Patch0:     0001-Sanitize.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  meson

BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(ModemManager)
BuildRequires:  pkgconfig(mm-glib)
BuildRequires:  pkgconfig(libpulse-mainloop-glib)


%description
A daemon to start/stop PulseAudio loopbacks for phone call audio
During a phone call, audio must be streamed from one ALSA device to
another ALSA device.  This should only happen during the call, when
the modem's audio interfaces will actually be active. To facilitate
this, Wys will wait for ModemManager calls in the ringing or active
state and enable appropriate PulseAudio loopback modules only during
those states.

%prep
%autosetup -p1 -n %{name}-v%{version}
	
%build
%meson
%meson_build
	
%install
%meson_install

%{__install} -Dpm 0644 %{SOURCE1} %{buildroot}%{_unitdir}/wys.service


%files
%{_bindir}/wys
%{_unitdir}/wys.service
%{_datadir}/machine-check/wys/blacklist


%doc README.md
%license COPYING

%changelog
* Tue Nov 10 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.1.9-1
- Initial packaging
